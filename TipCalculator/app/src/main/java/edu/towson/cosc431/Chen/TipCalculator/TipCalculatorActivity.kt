package edu.towson.cosc431.Chen.TipCalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_tip_calculator.*
import java.lang.Exception
import java.lang.NumberFormatException

class TipCalculator:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tip_calculator)

        calcBtn.setOnClickListener { calcTip() }

        radioGroup.setOnCheckedChangeListener { group, checkedId ->updateLabel(checkedId)  }
    }


    private fun updateLabel(checkedId:Int){
        val titleText = when(checkedId){
            radio10pct.id -> R.string.ten_pct
            radio20pct.id -> R.string.twenty_pct
            radiopct30.id -> R.string.thirty_pct

            else -> throw Exception("Unexpected Radio Button")
        }
         //title.text = resources.getText(titleText)
    }

    fun calcTip(){
        val inputString = amtInput.editableText.toString()
        val checkedId = radioGroup.checkedRadioButtonId

        val tempType = when(checkedId) {
            radio10pct.id -> TempType.Ten_pct
            radio20pct.id -> TempType.Twenty_pct
            radiopct30.id -> TempType.Thirty_pct

            else -> throw Exception("Unexpected Id ")
        }

        val result = calculate(tempType, inputString)

        when (result){

            null ->resultTextView.text = "Error"
            else -> resultTextView.text =String.format("Your calculated tip is $%.2f and your total is $%.2f", result, (inputString.toDouble() + result))


        }

    }

    fun calculate(tempType: TempType, strVaule: String): Double?{
        try {
            val result = strVaule.toDouble()

            return when (tempType){
                TempType.Ten_pct -> result * 0.10
                TempType.Twenty_pct -> result * 0.20
                TempType.Thirty_pct -> result * 0.30
            }
        }catch(e: NumberFormatException){
            return null
        }
    }
    enum class TempType{
        Ten_pct, Twenty_pct, Thirty_pct
    }
}


